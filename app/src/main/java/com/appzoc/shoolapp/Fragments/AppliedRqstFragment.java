package com.appzoc.shoolapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appzoc.shoolapp.Adapters.AppliedRqstAdapter;
import com.appzoc.shoolapp.Adapters.CommunicationAdapter;
import com.appzoc.shoolapp.Models.AppliedRqstData;
import com.appzoc.shoolapp.Models.CommunicationData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AppliedRqstFragment extends Fragment {

    RecyclerView recyclerview_applied_rqst;
    AppliedRqstAdapter adapter;
    ArrayList<AppliedRqstData> arr_appliedRqst=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_applied_rqst, container, false);
        initilize(v);
        return v;
    }

    public void initilize(View v){
        recyclerview_applied_rqst=(RecyclerView) v.findViewById(R.id.recyclerview_applied_rqst);
        setAdapter();
    }

    public void setAdapter(){
        adapter=new AppliedRqstAdapter(arr_appliedRqst,getActivity());
        recyclerview_applied_rqst.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerview_applied_rqst.setAdapter(adapter);
    }

}
