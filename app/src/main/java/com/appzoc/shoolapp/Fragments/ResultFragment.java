package com.appzoc.shoolapp.Fragments;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.appzoc.shoolapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends Fragment {

    TableLayout tab,tablelayout1,tab2;
    public static DecimalFormat format;
    LinearLayout lin,lin_main,lintab;
    ArrayList<String> sub_arr=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      View v= inflater.inflate(R.layout.fragment_result, container, false);

        format=new DecimalFormat("##########");
        format.setMaximumFractionDigits(2);

        initilize(v);
      return v;
    }

    public void initilize(View view){
        lin_main=(LinearLayout) view.findViewById(R.id.lin_main);
        tablelayout1=(TableLayout) view.findViewById(R.id.table_layout1);
        tab=(TableLayout) view.findViewById(R.id.tab);
        tab2=(TableLayout)view.findViewById(R.id.tab2);
        //lintab=(LinearLayout) view.findViewById(R.id.lintab);

        loadArry();

        setTable();
    }


    public void loadArry(){
        sub_arr.clear();
        sub_arr.add("english");
        sub_arr.add("maths");
        sub_arr.add("tamil");
        sub_arr.add("hindhi") ;
        sub_arr.add("computer");
        sub_arr.add("chemistry");

        sub_arr.add("english");
        sub_arr.add("maths");
        sub_arr.add("tamil");
        sub_arr.add("hindhi") ;
        sub_arr.add("computer");
        sub_arr.add("chemistry");
    }

    public void setTable(){
        tablelayout1.removeAllViews();
        tab.removeAllViews();
        tab2.removeAllViews();
        int a_sub,a_obtMrk,a_totMrk,a_percen;
        int b_sub=0,b_obtMrk=0,b_totMrk=0,b_percen=0;

        int t_sub,t_obtMrk,t_totMrk,t_percen;
        int tot_tx,tot_obtMrk,tot_tot,tot_percen;

        TableRow trow = new TableRow(getActivity());
        TableRow trow1 = new TableRow(getActivity());

        TextView txt1_t = new TextView(getActivity());
        TextView txt2_t = new TextView(getActivity());
        TextView txt3_t = new TextView(getActivity());
        TextView txt4_t = new TextView(getActivity());


        TextView txt11 = new TextView(getActivity());
        TextView txt22 = new TextView(getActivity());
        TextView txt33 = new TextView(getActivity());
        TextView txt44 = new TextView(getActivity());

        txt1_t.setPadding(10,10,10,10);
        txt2_t.setPadding(10,10,10,10);
        txt3_t.setPadding(10,10,10,10);
        txt4_t.setPadding(10,10,10,10);

        txt11.setPadding(10, 10, 10, 10);
        txt22.setPadding(10, 10, 10, 10);
        txt33.setPadding(10, 10, 10, 10);
        txt44.setPadding(10, 10, 10, 10);

        txt11.setGravity(Gravity.CENTER);
        txt22.setGravity(Gravity.CENTER);
        txt33.setGravity(Gravity.CENTER);
        txt44.setGravity(Gravity.CENTER);

        txt1_t.setText("Subject");
        txt2_t.setText("Obtained"+"\n"+"Marks");
        txt3_t.setText("Total"+"\n"+"Marks");
        txt4_t.setText("Percentage");

        txt11.setText("Total ");
        txt22.setText("500 ");
        txt33.setText("600");
        txt44.setText("85%");

        txt1_t.setTextColor(Color.parseColor("#ffffff"));
        txt2_t.setTextColor(Color.parseColor("#ffffff"));
        txt3_t.setTextColor(Color.parseColor("#ffffff"));
        txt4_t.setTextColor(Color.parseColor("#ffffff"));


        txt11.setTextColor(getResources().getColor(R.color.white));
        txt22.setTextColor(getResources().getColor(R.color.black));
        txt33.setTextColor(getResources().getColor(R.color.black));
        txt44.setTextColor(getResources().getColor(R.color.black));

        txt11.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        txt1_t.setTypeface(null, Typeface.BOLD);
        txt2_t.setTypeface(null, Typeface.BOLD);
        txt3_t.setTypeface(null, Typeface.BOLD);
        txt4_t.setTypeface(null, Typeface.BOLD);

        txt11.setTypeface(null, Typeface.BOLD);
        txt11.setTextSize(16);
        txt22.setTypeface(null, Typeface.BOLD);
        txt22.setTextSize(16);
        txt33.setTypeface(null, Typeface.BOLD);
        txt33.setTextSize(16);
        txt44.setTypeface(null, Typeface.BOLD);
        txt44.setTextSize(16);

        txt1_t.setGravity(Gravity.CENTER);
        txt2_t.setGravity(Gravity.CENTER);
        txt3_t.setGravity(Gravity.CENTER);
        txt4_t.setGravity(Gravity.CENTER);

        trow.addView(txt1_t); ;
        trow.addView(txt2_t);
        trow.addView(txt3_t);
        trow.addView(txt4_t);



        trow.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        //trow1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        trow1.setDividerDrawable(getResources().getDrawable(R.drawable.columndivider));
        trow1.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        trow.setDividerDrawable(getResources().getDrawable(R.drawable.columndivider));
        trow.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

        trow1.addView(txt11);
        trow1.addView(txt22);
        trow1.addView(txt33);
        trow1.addView(txt44);


        tablelayout1.addView(trow);

        txt11.measure(0, 0);
        txt22.measure(0, 0);
        txt33.measure(0, 0);
        txt44.measure(0, 0);

        tot_tx=txt11.getMeasuredWidth();
        tot_obtMrk=txt22.getMeasuredWidth();
        tot_tot=txt33.getMeasuredWidth();
        tot_percen=txt44.getMeasuredWidth();


        // if (c.getCount() != 0) {

        for (int i = 0; i < sub_arr.size(); i++) {

            trow = new TableRow(getActivity());
            TextView txt1 = new TextView(getActivity());
            TextView txt2 = new TextView(getActivity());
            TextView txt3 = new TextView(getActivity());
            TextView txt4 = new TextView(getActivity());

            txt1.setPadding(10, 10, 10, 10);
            txt2.setPadding(10,10,10,10);
            txt3.setPadding(10,10,10,10);
            txt4.setPadding(10,10,10,10);

            txt1.setGravity(Gravity.CENTER);
            txt2.setGravity(Gravity.CENTER);
            txt3.setGravity(Gravity.CENTER);
            txt4.setGravity(Gravity.CENTER);

            txt1.setTextColor(getResources().getColor(R.color.black));
            txt2.setTextColor(getResources().getColor(R.color.black));
            txt3.setTextColor(getResources().getColor(R.color.black));
            txt4.setTextColor(getResources().getColor(R.color.black));
            //txt4.setTextColor(Color.parseColor("#004d38"));

            txt1.setText(sub_arr.get(i));
            txt2.setText("85");
            txt3.setText("100");
            //String quantity=format.format(85);
            txt4.setText("85");

            trow.setDividerDrawable(getResources().getDrawable(R.drawable.columndivider));
            trow.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);


            trow.addView(txt1);
            trow.addView(txt2);
            trow.addView(txt3);
            trow.addView(txt4);

            txt1.measure(0, 0);
            txt2.measure(0, 0);
            txt3.measure(0,0);
            txt4.measure(0,0);


            a_sub=txt1.getMeasuredWidth();
            a_obtMrk=txt2.getMeasuredWidth();
            a_totMrk=txt3.getMeasuredWidth();
            a_percen=txt4.getMeasuredWidth();


            if(a_sub>b_sub){
                b_sub=a_sub;
            }
            if(a_obtMrk>b_obtMrk){
                b_obtMrk=a_obtMrk;
            }
            if(a_totMrk>b_totMrk){
                b_totMrk=a_totMrk;
            }
            if(a_percen>b_percen){
                b_percen=a_percen;
            }


            txt1_t.measure(0, 0);
            txt2_t.measure(0, 0);
            txt3_t.measure(0,0);
            txt4_t.measure(0,0);



            t_sub=txt1_t.getMeasuredWidth();
            t_obtMrk=txt2_t.getMeasuredWidth();
            t_totMrk=txt3_t.getMeasuredWidth();
            t_percen=txt4_t.getMeasuredWidth();



            if((b_sub>t_sub)&&(b_sub>tot_tx)){
                txt1.setWidth( b_sub);

                txt1_t.setWidth( b_sub);
                txt11.setWidth( b_sub);

            }else if((t_sub>b_sub)&&(t_sub>tot_tx)){
                txt1.setWidth( t_sub);

                txt1_t.setWidth( t_sub);
                txt11.setWidth( t_sub);

            }else if(( tot_tx>b_sub)&&( tot_tx>t_sub)){
                txt1.setWidth( tot_tx);

                txt1_t.setWidth( tot_tx);
                txt11.setWidth( tot_tx);
            }



            if((b_obtMrk>t_obtMrk)&&(b_obtMrk>tot_obtMrk)){
                txt2.setWidth( b_obtMrk);

                txt2_t.setWidth( b_obtMrk);
                txt22.setWidth( b_obtMrk);
            }else if((t_obtMrk>b_obtMrk)&&(t_obtMrk>tot_obtMrk)){
                txt2.setWidth( t_obtMrk);

                txt2_t.setWidth( t_obtMrk);
                txt22.setWidth( t_obtMrk);

            }else if(( tot_obtMrk>b_obtMrk)&&( tot_obtMrk>t_obtMrk)){
                txt2.setWidth( tot_obtMrk);

                txt2_t.setWidth( tot_obtMrk);
                txt22.setWidth( tot_obtMrk);
            }



            if((b_totMrk>t_totMrk)&&(b_totMrk>tot_tot)){
                txt3.setWidth( b_totMrk);

                txt3_t.setWidth( b_totMrk);
                txt33.setWidth( b_totMrk);
            }else if((t_totMrk>b_totMrk)&&(t_totMrk>tot_tot)){
                txt3.setWidth( t_totMrk);

                txt3_t.setWidth( t_totMrk);
                txt33.setWidth( t_totMrk);

            }else if(( tot_tot>b_totMrk)&&( tot_tot>t_totMrk)){
                txt3.setWidth( tot_tot);

                txt3_t.setWidth( tot_tot);
                txt33.setWidth( tot_tot);
            }


            if((b_percen>t_percen)&&(b_percen>tot_percen)){
                txt4.setWidth( b_percen);

                txt4_t.setWidth( b_percen);
                txt44.setWidth( b_percen);
            }else if((t_percen>b_percen)&&(t_percen>tot_percen)){
                txt4.setWidth( t_percen);

                txt4_t.setWidth( t_percen);
                txt44.setWidth( t_percen);


            }else if(( tot_percen>b_percen)&&( tot_percen>t_percen)){
                txt4.setWidth( tot_percen);

                txt4_t.setWidth( tot_percen);
                txt44.setWidth( tot_percen);

                       }


            tab.addView(trow);

        }

        tab2.addView(trow1);

    }

}
