package com.appzoc.shoolapp.Fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.Activites.LeaveCalendar;
import com.appzoc.shoolapp.Adapters.PendingFeeAdapter;
import com.appzoc.shoolapp.Models.PendingFeeData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PendingFeeFragment extends Fragment {

    RecyclerView recyclvw_pendingFee;
    PendingFeeAdapter adapter;
    ArrayList<PendingFeeData> arr_pendFee=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_pending_fee, container, false);
        initilize(v);

        return v;

    }

    public void initilize(View v){

        recyclvw_pendingFee=(RecyclerView) v.findViewById(R.id.recyclvw_pendingFee);
        setAdapter();
    }

    public void setAdapter(){
        adapter=new PendingFeeAdapter(arr_pendFee,getActivity());
        recyclvw_pendingFee.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclvw_pendingFee.setAdapter(adapter);
    }



}
