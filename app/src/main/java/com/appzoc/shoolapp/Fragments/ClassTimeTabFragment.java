package com.appzoc.shoolapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appzoc.shoolapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClassTimeTabFragment extends Fragment {


    public ClassTimeTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_class_time_tab, container, false);
    }

}
