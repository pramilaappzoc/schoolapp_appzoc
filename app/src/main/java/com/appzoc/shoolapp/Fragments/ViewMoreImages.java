package com.appzoc.shoolapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appzoc.shoolapp.Adapters.ViewMoreImgAdapter;
import com.appzoc.shoolapp.Models.ImageItemData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewMoreImages extends Fragment {
    RecyclerView recycler_view_vimage;
    ViewMoreImgAdapter adapter;
    private ArrayList<ImageItemData> imageItems_arr=new ArrayList<>();
    TextView eventTitle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View v=inflater.inflate(R.layout.fragment_view_more_images, container, false);
         initilize(v);

         return v;

    }

    public void initilize(View view){


        eventTitle=(TextView)view.findViewById(R.id.eventTitle);

        recycler_view_vimage = (RecyclerView)view.findViewById(R.id.recycler_view_vimage);
        recycler_view_vimage.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        recycler_view_vimage.setLayoutManager(layoutManager);
        setAdapter();

    }

    public void setAdapter(){
        //loadMenuList();
        adapter=new ViewMoreImgAdapter(imageItems_arr,getActivity());
        recycler_view_vimage.setAdapter(adapter);
    }

}
