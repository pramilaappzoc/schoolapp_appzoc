package com.appzoc.shoolapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appzoc.shoolapp.Adapters.ExamSchedAdapter;
import com.appzoc.shoolapp.Adapters.PaidFeeAdapter;
import com.appzoc.shoolapp.Models.ExamSchedData;
import com.appzoc.shoolapp.Models.PaidFeeData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExamSched extends Fragment {


    RecyclerView recyclvw_examSched;
    ExamSchedAdapter adapter;
    ArrayList<ExamSchedData> arr_examSched=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View v=inflater.inflate(R.layout.fragment_exam_sched, container, false);
         initilize(v);
         return v;
    }

    public void initilize(View v){

        recyclvw_examSched=(RecyclerView) v.findViewById(R.id.recyclvw_examSched);
        setAdapter();
    }

    public void setAdapter(){
        adapter=new ExamSchedAdapter(arr_examSched,getActivity());
        recyclvw_examSched.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclvw_examSched.setAdapter(adapter);
    }


}
