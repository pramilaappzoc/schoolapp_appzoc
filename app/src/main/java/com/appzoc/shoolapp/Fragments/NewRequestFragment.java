package com.appzoc.shoolapp.Fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.Activites.LeaveCalendar;
import com.appzoc.shoolapp.Activites.MainActivity;
import com.appzoc.shoolapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewRequestFragment extends Fragment implements View.OnClickListener{

    Button btn_apply;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v= inflater.inflate(R.layout.fragment_new_request, container, false);
       initilize(v);
        return v;
    }

    public void initilize(View v){
        btn_apply=(Button) v.findViewById(R.id.btn_apply);

        btn_apply.setOnClickListener(this);


    }




    @Override
    public void onClick(View view) {
     int id=view.getId();
     switch (id){
         case R.id.btn_apply: {
             popupthankyou();
         }break;
     }

    }


    private void popupthankyou() {
        final Dialog dialog = new Dialog(getActivity(), R.style.MyDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);

        LayoutInflater m_inflater = LayoutInflater.from(getActivity());
        View m_view = m_inflater.inflate(R.layout.submited_popup, null);
        TextView txt_submit=(TextView) m_view.findViewById(R.id.txt_submit);


        ImageView submit = (ImageView) m_view.findViewById(R.id.img_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                        Intent intent=new Intent(getActivity(),LeaveCalendar.class);
                        intent.putExtra("fragement","1");
                        startActivity(intent);
                        getActivity().finish();

            }
        });
        dialog.setContentView(m_view);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.show();

    }
}
