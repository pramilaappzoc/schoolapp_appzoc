package com.appzoc.shoolapp.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.appzoc.shoolapp.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExamSchedule extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    Toolbar toolbar;
    ImageView img_back;
    int tabpos=0;
    ArrayList<String> tabtiles,tabids;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_exam_schedule, container, false);
        initilize(v);
        return v;
    }

    public void initilize(View view){

        viewPager = (ViewPager) view.findViewById(R.id.viewpager1);
        tabtiles=new ArrayList<>();


        loadArray();
        setupViewPager(viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

/*
        tabids=getIntent().getStringArrayListExtra("tabids");
        tabtiles=getIntent().getStringArrayListExtra("tabtitle");
        tabpos=getIntent().getIntExtra("FragmentPosition",0);*/


        viewPager.setCurrentItem(tabpos);

    }

    public void loadArray(){
        tabtiles.clear();
        for(int i=1;i<4;i++) {
            tabtiles.add("term"+i);
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        LayoutInflater inflator = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        int count = tabtiles.size();
        for (int i=0; i<count; i++){

//            Log.e("TAG",tabids.get(i)+"");

           /* Bundle bundle = new Bundle();

            bundle.putString("CatID",tabids.get(i)+"");
*/
            ExamSched fView = new ExamSched();
            //  fView.setArguments(bundle);
            adapter.addFrag(fView,tabtiles.get(i));
        }

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
