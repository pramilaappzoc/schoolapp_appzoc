package com.appzoc.shoolapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appzoc.shoolapp.Adapters.AppliedRqstAdapter;
import com.appzoc.shoolapp.Adapters.ResidentialAddressAdapter;
import com.appzoc.shoolapp.Models.AppliedRqstData;
import com.appzoc.shoolapp.Models.ResAddressData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResidentialAddress extends Fragment {

    RecyclerView recyclerview_address;
    ResidentialAddressAdapter adapter;
    ArrayList<ResAddressData> arr_address=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_residential_address, container, false);
        initilize(view);
        return view;
    }

    public void initilize(View view){
        recyclerview_address=(RecyclerView) view.findViewById(R.id.recyclerview_address);
        setAdapter();
    }

    public void setAdapter(){
        adapter=new ResidentialAddressAdapter(arr_address,getActivity());
        recyclerview_address.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerview_address.setAdapter(adapter);
    }


}
