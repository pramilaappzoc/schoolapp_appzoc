package com.appzoc.shoolapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appzoc.shoolapp.Adapters.PaidFeeAdapter;
import com.appzoc.shoolapp.Adapters.PendingFeeAdapter;
import com.appzoc.shoolapp.Models.PaidFeeData;
import com.appzoc.shoolapp.Models.PendingFeeData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PaidFeeFragment extends Fragment {

    RecyclerView recyclvw_paidFee;
    PaidFeeAdapter adapter;
    ArrayList<PaidFeeData> arr_paidFee=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v= inflater.inflate(R.layout.fragment_paid_fee, container, false);
       initilize(v);
       return v;
    }

    public void initilize(View v){

        recyclvw_paidFee=(RecyclerView) v.findViewById(R.id.recyclvw_paidFee);
        setAdapter();
    }

    public void setAdapter(){
        adapter=new PaidFeeAdapter(arr_paidFee,getActivity());
        recyclvw_paidFee.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclvw_paidFee.setAdapter(adapter);
    }

}
