package com.appzoc.shoolapp.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Activites.Fees;
import com.appzoc.shoolapp.Models.PendingFeeData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 28/4/18.
 */

public class PendingFeeAdapter extends RecyclerView.Adapter<PendingFeeAdapter.ItemRowHolder>{

    ArrayList<PendingFeeData> arr_pendFee=new ArrayList<>();
    Context context;

    public PendingFeeAdapter(ArrayList<PendingFeeData> arr_pendFee, Context context) {
        this.arr_pendFee = arr_pendFee;
        this.context = context;
    }


    @Override
    public PendingFeeAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pendingfee_list_item, null);
        PendingFeeAdapter.ItemRowHolder mh = new PendingFeeAdapter.ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(PendingFeeAdapter.ItemRowHolder holder, int position) {

       holder.btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupthankyou();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView title;
        ImageView imageView;
        View divider;
        LinearLayout linearSearch;
        Button btn_pay;



        public ItemRowHolder(View view) {
            super(view);

          /*  this.title = (TextView) view.findViewById(R.id.itemTitle);
            this.imageView = (ImageView) view.findViewById(R.id.itemImage);
            this.divider=(View)view.findViewById(R.id.itemView);
            this.linearSearch=(LinearLayout) view.findViewById(R.id.linearSearch);*/
            this.btn_pay=(Button) view.findViewById(R.id.btn_pay);

        }


    }

    private void popupthankyou() {
        final Dialog dialog = new Dialog(context, R.style.MyDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);

        LayoutInflater m_inflater = LayoutInflater.from(context);
        View m_view = m_inflater.inflate(R.layout.submited_popup, null);
        TextView txt_submit=(TextView) m_view.findViewById(R.id.txt_submit);


        ImageView submit = (ImageView) m_view.findViewById(R.id.img_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent=new Intent(context,Fees.class);
                intent.putExtra("callfragement","1");
                context.startActivity(intent);
                ((Activity)context).finish();

            }
        });
        dialog.setContentView(m_view);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.show();

    }
}
