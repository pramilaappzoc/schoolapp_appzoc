package com.appzoc.shoolapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Activites.MainActivity;
import com.appzoc.shoolapp.Models.ImageItemData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 30/4/18.
 */

public class EventImageAdapter extends RecyclerView.Adapter<EventImageAdapter.SingleItemRowHolder> {
    private ArrayList<ImageItemData> imageItems_arr;
    private Context mContext;

    public EventImageAdapter(ArrayList<ImageItemData> imageItems_arr, Context mContext) {
        this.imageItems_arr = imageItems_arr;
        this.mContext = mContext;
    }

    @Override
    public EventImageAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_list_item, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(EventImageAdapter.SingleItemRowHolder holder, int position) {

        ImageItemData singleItem = imageItems_arr.get(position);

        //holder.tvTitle.setText(singleItem.getName());


       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != imageItems_arr ? imageItems_arr.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

       // protected TextView tvTitle;

        protected ImageView itemImage;


        public SingleItemRowHolder(View view) {
            super(view);

           // this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();
                    popupPhoto();
                }
            });


        }



    }

    private void popupPhoto() {
        final Dialog dialog = new Dialog(mContext, R.style.photoDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);

        LayoutInflater m_inflater = LayoutInflater.from(mContext);
        View m_view = m_inflater.inflate(R.layout.view_image_dialog, null);


        ImageView submit = (ImageView) m_view.findViewById(R.id.img_phto);
        RelativeLayout rel=(RelativeLayout)m_view.findViewById(R.id.rel_photo);
        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                        /*Intent intent=new Intent(ChatRoomActivity.this,AllTasksActivity.class);
                        startActivity(intent);*/

            }
        });
        dialog.setContentView(m_view);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        dialog.getWindow().setBackgroundDrawableResource(R.color.translucentblue);
        dialog.show();

    }
}
