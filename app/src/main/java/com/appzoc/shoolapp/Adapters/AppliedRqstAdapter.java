package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Models.AppliedRqstData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 27/4/18.
 */

public class AppliedRqstAdapter extends RecyclerView.Adapter<AppliedRqstAdapter.ItemRowHolder> {

    public AppliedRqstAdapter(ArrayList<AppliedRqstData> arr_appliedRqst, Context context) {
        this.arr_appliedRqst = arr_appliedRqst;
        this.context = context;
    }

    ArrayList<AppliedRqstData> arr_appliedRqst=new ArrayList<>();
    Context context;

    @Override
    public AppliedRqstAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.applied_rqst_list_item, null);
        AppliedRqstAdapter.ItemRowHolder mh = new AppliedRqstAdapter.ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(AppliedRqstAdapter.ItemRowHolder holder, int position) {

        if (position == 0) {
            holder.img_status.setImageResource(R.drawable.green_tick);
        }else if(position==1){
            holder.img_status.setImageResource(R.drawable.alarm_clock);
        }
        else {  holder.img_status.setImageResource(R.drawable.cancel); }

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {


        ImageView img_status;
        View divider;
        LinearLayout linearSearch;



        public ItemRowHolder(View view) {
            super(view);
            this.img_status = (ImageView) view.findViewById(R.id.img_status);
          /*  this.title = (TextView) view.findViewById(R.id.itemTitle);

            this.divider=(View)view.findViewById(R.id.itemView);
            this.linearSearch=(LinearLayout) view.findViewById(R.id.linearSearch);

*/

        }


    }
}
