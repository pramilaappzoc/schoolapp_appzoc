package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 9/5/18.
 */

public class MailSubAdapter extends RecyclerView.Adapter<MailSubAdapter.ItemRowHolder>{

    Context mcontext;
    ArrayList<String> arr_mailSub=new ArrayList<>();
    ArrayList<String> color_arr=new ArrayList<>();
    int row_index;

    public MailSubAdapter(Context mcontext, ArrayList<String> arr_mailSub) {
        this.mcontext = mcontext;
        this.arr_mailSub = arr_mailSub;
    }


    @Override
    public MailSubAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.mail_sub_item, null);
        MailSubAdapter.ItemRowHolder mh = new MailSubAdapter.ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(final MailSubAdapter.ItemRowHolder holder, final int position) {

        holder.txt_mailSub.setText(arr_mailSub.get(position));
        holder.txt_mailSub.setId(position);


        holder.txt_mailSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=position;
                notifyDataSetChanged();
            }
        });

        
        if (row_index==position) {
            holder.txt_mailSub.setTextColor(mcontext.getResources().getColor(R.color.translucentblue));
        } else {
            holder.txt_mailSub.setTextColor(mcontext.getResources().getColor(R.color.darkgrey));
        }
    }

    @Override
    public int getItemCount() {
        return arr_mailSub.size();
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {


        TextView txt_mailSub;



        public ItemRowHolder(View view) {
            super(view);

            this.txt_mailSub = (TextView) view.findViewById(R.id.txt_mailSub);


        }

    }
}
