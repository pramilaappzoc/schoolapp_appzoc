package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.appzoc.shoolapp.Models.CommunicationData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 26/4/18.
 */

public class CommunicationAdapter extends RecyclerView.Adapter<CommunicationAdapter.ItemRowHolder>{
    ArrayList<CommunicationData> arr_communi=new ArrayList<>();
    Context context;

    public CommunicationAdapter(ArrayList<CommunicationData> arr_communi, Context context) {
        this.arr_communi = arr_communi;
        this.context = context;
    }

    @Override
    public CommunicationAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.communication_list_item, null);
        CommunicationAdapter.ItemRowHolder mh = new CommunicationAdapter.ItemRowHolder(v);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        return mh;
    }

    @Override
    public void onBindViewHolder(CommunicationAdapter.ItemRowHolder holder, int position) {
              holder.img_email.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      sendMail("example@gmail.com");
                  }
              });

        holder.img_email_below.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMail("example@gmail.com");
            }
        });
    }

    @Override
    public int getItemCount() {
        return 2;
       // return arr_communi.size();
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView img_email,img_email_below;
        View divider;
        LinearLayout linearSearch;



        public ItemRowHolder(View view) {
            super(view);


            this.img_email = (ImageView) view.findViewById(R.id.img_email);
            this.img_email_below = (ImageView) view.findViewById(R.id.img_email_below);
              /*  this.title = (TextView) view.findViewById(R.id.itemTitle);
            this.divider=(View)view.findViewById(R.id.itemView);
            this.linearSearch=(LinearLayout) view.findViewById(R.id.linearSearch);

*/
        }
    }


    public void sendMail(String mailId){

     /*       String to=editTextTo.getText().toString();
            String subject=editTextSubject.getText().toString();
            String message=editTextMessage.getText().toString();*/


        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{ mailId});
        email.putExtra(Intent.EXTRA_SUBJECT, "mail");
        // email.putExtra(Intent.EXTRA_TEXT, message);

        //need this to prompts email client only
        email.setType("message/rfc822");

        context.startActivity(Intent.createChooser(email, "Choose an Email client :"));
    }
}
