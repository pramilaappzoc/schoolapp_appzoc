package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appzoc.shoolapp.Models.ExamSchedData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 5/5/18.
 */

public class ExamSchedAdapter extends RecyclerView.Adapter<ExamSchedAdapter.ItemRowHolder>{

    ArrayList<ExamSchedData> arr_examSched=new ArrayList<>();
    Context context;

    public ExamSchedAdapter(ArrayList<ExamSchedData> arr_examSched, Context context) {
        this.arr_examSched = arr_examSched;
        this.context = context;
    }


    @Override
    public ExamSchedAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_sched_list_item, null);
        ExamSchedAdapter.ItemRowHolder mh = new ExamSchedAdapter.ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ExamSchedAdapter.ItemRowHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        /*protected TextView title;
        ImageView imageView;
        View divider;
        LinearLayout linearSearch;
        Button btn_pay;*/



        public ItemRowHolder(View view) {
            super(view);

          /*  this.title = (TextView) view.findViewById(R.id.itemTitle);
            this.imageView = (ImageView) view.findViewById(R.id.itemImage);
            this.divider=(View)view.findViewById(R.id.itemView);
            this.linearSearch=(LinearLayout) view.findViewById(R.id.linearSearch);*/

        }


    }
}
