package com.appzoc.shoolapp.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appzoc.shoolapp.Models.PaidFeeData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 28/4/18.
 */

public class PaidFeeAdapter extends RecyclerView.Adapter<PaidFeeAdapter.ItemRowHolder>{
    ArrayList<PaidFeeData> arr_paidFee=new ArrayList<>();
    Context context;

    public PaidFeeAdapter(ArrayList<PaidFeeData> arr_paidFee, Context context) {
        this.arr_paidFee = arr_paidFee;
        this.context = context;
    }


    @Override
    public PaidFeeAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.paidfee_list_item, null);
        PaidFeeAdapter.ItemRowHolder mh = new PaidFeeAdapter.ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(PaidFeeAdapter.ItemRowHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        /*protected TextView title;
        ImageView imageView;
        View divider;
        LinearLayout linearSearch;
        Button btn_pay;*/



        public ItemRowHolder(View view) {
            super(view);

          /*  this.title = (TextView) view.findViewById(R.id.itemTitle);
            this.imageView = (ImageView) view.findViewById(R.id.itemImage);
            this.divider=(View)view.findViewById(R.id.itemView);
            this.linearSearch=(LinearLayout) view.findViewById(R.id.linearSearch);*/

        }


    }
}