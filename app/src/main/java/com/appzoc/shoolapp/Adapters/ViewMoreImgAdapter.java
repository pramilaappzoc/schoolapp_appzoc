package com.appzoc.shoolapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.appzoc.shoolapp.Models.ImageItemData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 30/4/18.
 */

public class ViewMoreImgAdapter extends RecyclerView.Adapter<ViewMoreImgAdapter.ItemRowHolder>{

    ArrayList<ImageItemData> imageItems_arr=new ArrayList<>();
    private Context mContext;


    public ViewMoreImgAdapter(ArrayList<ImageItemData> imageItems_arr, Context mContext) {
        this.imageItems_arr = imageItems_arr;
        this.mContext = mContext;
    }



    @Override
    public ViewMoreImgAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ViewMoreImgAdapter.ItemRowHolder holder, int position) {
//        ImageItemData singleItem = imageItems_arr.get(position);
    }

    @Override
    public int getItemCount() {
        return 8;
    }


    public class ItemRowHolder extends RecyclerView.ViewHolder {

        // protected TextView tvTitle;

        protected ImageView itemImage;


        public ItemRowHolder(View view) {
            super(view);

            // this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();
                    popupPhoto();
                }
            });


        }

    }


    private void popupPhoto() {
        final Dialog dialog = new Dialog(mContext, R.style.photoDialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);

        LayoutInflater m_inflater = LayoutInflater.from(mContext);
        View m_view = m_inflater.inflate(R.layout.view_image_dialog, null);


        ImageView submit = (ImageView) m_view.findViewById(R.id.img_phto);
        RelativeLayout rel=(RelativeLayout)m_view.findViewById(R.id.rel_photo);
        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                        /*Intent intent=new Intent(ChatRoomActivity.this,AllTasksActivity.class);
                        startActivity(intent);*/

            }
        });
        dialog.setContentView(m_view);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        dialog.getWindow().setBackgroundDrawableResource(R.color.translucentblue);
        dialog.show();

    }

}
