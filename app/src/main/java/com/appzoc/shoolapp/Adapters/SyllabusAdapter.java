package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Activites.SyllabusInDetail;
import com.appzoc.shoolapp.Models.syllabusData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 27/4/18.
 */

public class SyllabusAdapter extends RecyclerView.Adapter<SyllabusAdapter.ItemRowHolder>{
    public SyllabusAdapter(Context context, ArrayList<syllabusData> arr_syllabus) {
        this.context = context;
        this.arr_syllabus = arr_syllabus;
    }

    Context context;
    ArrayList<syllabusData> arr_syllabus=new ArrayList<>();
    @Override
    public SyllabusAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.syllabus_list_item, null);
        SyllabusAdapter.ItemRowHolder mh = new SyllabusAdapter.ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SyllabusAdapter.ItemRowHolder holder, int position) {
    holder.cardview.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i=new Intent(context, SyllabusInDetail.class);
            i.putExtra("subject_name","Physics");
            context.startActivity(i);
        }
    });

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView title;
        ImageView imageView;
        View divider;
        LinearLayout linearSearch;
        CardView cardview;


        public ItemRowHolder(View view) {
            super(view);

          /*  this.title = (TextView) view.findViewById(R.id.itemTitle);
            this.imageView = (ImageView) view.findViewById(R.id.itemImage);
            this.divider=(View)view.findViewById(R.id.itemView);
            this.linearSearch=(LinearLayout) view.findViewById(R.id.linearSearch);
*/
            this.cardview=(CardView)view.findViewById(R.id.cardview);

        }


    }
}
