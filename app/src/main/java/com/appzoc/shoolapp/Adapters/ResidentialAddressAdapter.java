package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Models.ResAddressData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 4/5/18.
 */

public class ResidentialAddressAdapter extends RecyclerView.Adapter<ResidentialAddressAdapter.ItemRowHolder> {
    ArrayList<ResAddressData> arr_address=new ArrayList<>();
    Context context;

    public ResidentialAddressAdapter(ArrayList<ResAddressData> arr_address, Context context) {
        this.arr_address = arr_address;
        this.context = context;
    }

    @Override
    public ResidentialAddressAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.res_address_list_item, null);
        ResidentialAddressAdapter.ItemRowHolder mh = new ResidentialAddressAdapter.ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ResidentialAddressAdapter.ItemRowHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        ImageView img_status;
        View divider;
        LinearLayout linearSearch;



        public ItemRowHolder(View view) {
            super(view);
          /*  this.title = (TextView) view.findViewById(R.id.itemTitle);

            this.divider=(View)view.findViewById(R.id.itemView);
            this.linearSearch=(LinearLayout) view.findViewById(R.id.linearSearch);

*/

        }


    }
}
