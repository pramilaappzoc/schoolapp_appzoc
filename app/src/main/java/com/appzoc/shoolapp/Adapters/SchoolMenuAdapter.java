package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.appzoc.shoolapp.Activites.Attendance;
import com.appzoc.shoolapp.Activites.Communicate;
import com.appzoc.shoolapp.Activites.Fees;
import com.appzoc.shoolapp.Activites.Gallery;
import com.appzoc.shoolapp.Activites.LeaveCalendar;
import com.appzoc.shoolapp.Activites.Result;
import com.appzoc.shoolapp.Activites.Syllabus;
import com.appzoc.shoolapp.Activites.TimeTable;
import com.appzoc.shoolapp.Models.SchoolMenu;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 24/4/18.
 */

public class SchoolMenuAdapter extends RecyclerView.Adapter<SchoolMenuAdapter.ItemRowHolder>{
    Context mcontext;
    ArrayList<SchoolMenu> arr_schlMenu=new ArrayList<>();

    public SchoolMenuAdapter(Context mcontext, ArrayList<SchoolMenu> arr_schlMenu) {
        this.mcontext = mcontext;
        this.arr_schlMenu = arr_schlMenu;
    }


    @Override
    public SchoolMenuAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.school_menu_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SchoolMenuAdapter.ItemRowHolder holder, final int position) {

        holder.img_menu.setImageResource(arr_schlMenu.get(position).getCategoryImg());
        holder.txt_menu.setText(arr_schlMenu.get(position).getCategoryTxt());

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startEvent(position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return arr_schlMenu.size();
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        CardView cardview;
        TextView txt_menu;
        ImageView img_menu;


        public ItemRowHolder(View view) {
            super(view);

            this.txt_menu = (TextView) view.findViewById(R.id.txt_menu);
            this.img_menu = (ImageView) view.findViewById(R.id.img_menu);
            this.cardview=(CardView) view.findViewById(R.id.cardview);

        }

    }

    public void startEvent(int pos){
        switch (pos){
            case 0:{
                Intent i=new Intent(mcontext, Attendance.class);
                mcontext.startActivity(i);
            }break;
            case 1:{
                Intent i=new Intent(mcontext, TimeTable.class);
                mcontext.startActivity(i);
            }break;
            case 2:{
              Intent i=new Intent(mcontext, Communicate.class);
              mcontext.startActivity(i);

            }break;
            case 3:{
                Intent i=new Intent(mcontext, Result.class);
                mcontext.startActivity(i);
            }break;
            case 4:{
                Intent i=new Intent(mcontext, Syllabus.class);
                mcontext.startActivity(i);
            }break;
            case 5:{
                Intent i=new Intent(mcontext, Gallery.class);
                mcontext.startActivity(i);

            }break;
            case 6:{
                Intent i=new Intent(mcontext, LeaveCalendar.class);
                mcontext.startActivity(i);
            }break;
            case 7:{
                Intent i=new Intent(mcontext, Fees.class);
                mcontext.startActivity(i);
            }break;
        }
    }
}
