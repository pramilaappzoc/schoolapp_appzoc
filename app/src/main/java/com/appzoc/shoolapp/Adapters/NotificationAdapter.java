package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Models.NotiData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 26/4/18.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ItemRowHolder>{
    Context context;
    ArrayList<NotiData> arr_noti=new ArrayList<>();

    public NotificationAdapter(Context context, ArrayList<NotiData> arr_noti) {
        this.context = context;
        this.arr_noti = arr_noti;
    }

    @Override
    public NotificationAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, null);
        NotificationAdapter.ItemRowHolder mh = new NotificationAdapter.ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ItemRowHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        //return arr_noti.size();
        return 3;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView title;
        ImageView imageView;
        View divider;
        LinearLayout linearSearch;



        public ItemRowHolder(View view) {
            super(view);

          /*  this.title = (TextView) view.findViewById(R.id.itemTitle);
            this.imageView = (ImageView) view.findViewById(R.id.itemImage);
            this.divider=(View)view.findViewById(R.id.itemView);
            this.linearSearch=(LinearLayout) view.findViewById(R.id.linearSearch);

*/

        }


    }
}
