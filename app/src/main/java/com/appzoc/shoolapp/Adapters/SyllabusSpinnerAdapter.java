package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 8/5/18.
 */

public class SyllabusSpinnerAdapter extends ArrayAdapter<String> {
    ArrayList<String> arr_subject=new ArrayList<>();
    Context context;
    LayoutInflater inflater;

    public SyllabusSpinnerAdapter( Context context,int ResourceId,ArrayList<String> arr_subject) {
        super(context,ResourceId,arr_subject);
        this.context = context;
        this.arr_subject = arr_subject;

        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.syllabus_spin_item, parent, false);

        /***** Get each Model object from Arraylist ********/

        TextView sub          = (TextView)row.findViewById(R.id.txt);
        ImageView img = (ImageView)row.findViewById(R.id.img);

        sub.setText(arr_subject.get(position));
        img.setImageResource(R.drawable.ic_book_skyblue);

      /*  if(position==0){

            // Default selected Spinner item
            label.setText("Please select company");
            sub.setText("");
        }
        else
        {
            // Set values for spinner each row
            label.setText(tempValues.getCompanyName());
            sub.setText(tempValues.getUrl());
            companyLogo.setImageResource(res.getIdentifier
                    ("com.androidexample.customspinner:drawable/"
                            + tempValues.getImage(),null,null));

        }
*/
        return row;
    }
}
