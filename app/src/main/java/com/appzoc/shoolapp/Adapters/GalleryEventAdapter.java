package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Interfaces.CallMoreImages;
import com.appzoc.shoolapp.Models.GalleryEventData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 30/4/18.
 */

public class GalleryEventAdapter extends RecyclerView.Adapter<GalleryEventAdapter.ItemRowHolder>{
    ArrayList<GalleryEventData> galleryEven_arr;
    private Context mContext;
    CallMoreImages callMoreImages;

    public GalleryEventAdapter(ArrayList<GalleryEventData> galleryEven_arr, Context mContext) {
        this.galleryEven_arr = galleryEven_arr;
        this.mContext = mContext;
        callMoreImages=(CallMoreImages)mContext;
    }


    @Override
    public GalleryEventAdapter.ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_vertical_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(GalleryEventAdapter.ItemRowHolder holder, final int position) {

        final String EventName = galleryEven_arr.get(position).getEventTitle();

        ArrayList ImageItems = galleryEven_arr.get(position).getImageItemArr();

        holder.eventTitle.setText(EventName);
        EventImageAdapter imageItemListAdapter = new EventImageAdapter(ImageItems,mContext);

        holder.recycler_view_image.setHasFixedSize(true);
        holder.recycler_view_image.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        holder.recycler_view_image.setAdapter(imageItemListAdapter);

        holder.txtMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callMoreImages.mcallMoreImages(1);

            }
        });

            /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/

    }

    @Override
    public int getItemCount() {
        return (null != galleryEven_arr ? galleryEven_arr.size() : 0);
    }


    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView eventTitle,txtMore;
        protected RecyclerView recycler_view_image;
        LinearLayout lin;


        public ItemRowHolder(View view) {
            super(view);

            this.eventTitle = (TextView) view.findViewById(R.id.eventTitle);
            this.txtMore= (TextView) view.findViewById(R.id.txtMore);
            this.recycler_view_image = (RecyclerView) view.findViewById(R.id.recycler_view_image);
            this.lin=(LinearLayout)view.findViewById(R.id.lin);

        }

    }
}
