package com.appzoc.shoolapp.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.Models.EventData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

/**
 * Created by appzoc on 9/5/18.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ItemRowHolder>{

    Context context;
    ArrayList<EventData> arr_event=new ArrayList<>();

    public EventAdapter(Context context, ArrayList<EventData> arr_event) {
        this.context = context;
        this.arr_event = arr_event;
    }

    @Override
    public EventAdapter.ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(EventAdapter.ItemRowHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

      /*  CardView cardview;
        TextView txt_menu;
        ImageView img_menu;
*/

        public ItemRowHolder(View view) {
            super(view);

           /* this.txt_menu = (TextView) view.findViewById(R.id.txt_menu);
            this.img_menu = (ImageView) view.findViewById(R.id.img_menu);
            this.cardview=(CardView) view.findViewById(R.id.cardview);*/

        }

    }
}
