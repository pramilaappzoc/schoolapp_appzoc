package com.appzoc.shoolapp.Models;

/**
 * Created by appzoc on 30/4/18.
 */

public class ImageItemData {

    private String name;
    private String url;

    public ImageItemData(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
