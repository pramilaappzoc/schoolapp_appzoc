package com.appzoc.shoolapp.Models;

/**
 * Created by appzoc on 24/4/18.
 */

public class SchoolMenu {
    String categoryTxt;
    int categoryImg;

    public SchoolMenu(String categoryTxt, int categoryImg) {
        this.categoryTxt = categoryTxt;
        this.categoryImg = categoryImg;
    }


    public String getCategoryTxt() {
        return categoryTxt;
    }

    public void setCategoryTxt(String categoryTxt) {
        this.categoryTxt = categoryTxt;
    }

    public int getCategoryImg() {
        return categoryImg;
    }

    public void setCategoryImg(int categoryImg) {
        this.categoryImg = categoryImg;
    }


}
