package com.appzoc.shoolapp.Models;

import java.util.ArrayList;

/**
 * Created by appzoc on 30/4/18.
 */

public class GalleryEventData {
    private String EventTitle;
    private ArrayList<ImageItemData> ImageItemArr;

    public String getEventTitle() {
        return EventTitle;
    }

    public void setEventTitle(String eventTitle) {
        EventTitle = eventTitle;
    }

    public ArrayList<ImageItemData> getImageItemArr() {
        return ImageItemArr;
    }

    public void setImageItemArr(ArrayList<ImageItemData> imageItemArr) {
        ImageItemArr = imageItemArr;
    }




}
