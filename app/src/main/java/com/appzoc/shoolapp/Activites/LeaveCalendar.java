package com.appzoc.shoolapp.Activites;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Fragments.AppliedRqstFragment;
import com.appzoc.shoolapp.Fragments.NewRequestFragment;
import com.appzoc.shoolapp.R;

public class LeaveCalendar extends AppCompatActivity implements View.OnClickListener{
    TextView txt_newRqst,txt_appliedRqst;
    LinearLayout lin_fragment;
    Fragment fragment = null;
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_calendar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initilize();
      }

    public void initilize(){
        txt_newRqst=(TextView)findViewById(R.id.txt_newRqst);
        txt_appliedRqst=(TextView)findViewById(R.id.txt_appliedRqst);
        lin_fragment=(LinearLayout)findViewById(R.id.lin_fragment);
        img_back=(ImageView)findViewById(R.id.img_back);

        txt_newRqst.setOnClickListener(this);
        txt_appliedRqst.setOnClickListener(this);
        img_back.setOnClickListener(this);


        setfragment();
    }

    @Override
    public void onClick(View view) {
        int id= view.getId();
        switch (id){
            case R.id.txt_newRqst :{
                txt_newRqst.setTextColor(getResources().getColor(R.color.black));
                txt_appliedRqst.setTextColor(getResources().getColor(R.color.grey));
                fragment = new NewRequestFragment();
                callFragment();

            }break;

            case R.id.txt_appliedRqst:{
                txt_newRqst.setTextColor(getResources().getColor(R.color.grey));
                txt_appliedRqst.setTextColor(getResources().getColor(R.color.black));
                fragment = new AppliedRqstFragment();
                callFragment();
            }break;

            case R.id.img_back:{
                onBackPressed();
            }break;

        }

    }

    public void setfragment(){

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            String getName = (String) bd.get("fragement");
           if(getName.equals("1")){
               txt_newRqst.setTextColor(getResources().getColor(R.color.grey));
               txt_appliedRqst.setTextColor(getResources().getColor(R.color.black));
               fragment = new AppliedRqstFragment();
               callFragment();
           }else{
               txt_newRqst.setTextColor(getResources().getColor(R.color.black));
               txt_appliedRqst.setTextColor(getResources().getColor(R.color.grey));
               fragment = new NewRequestFragment();
               callFragment();
           }
        }else{
            txt_newRqst.setTextColor(getResources().getColor(R.color.black));
            txt_appliedRqst.setTextColor(getResources().getColor(R.color.grey));
            fragment = new NewRequestFragment();
            callFragment();
        }

    }

    public void callFragment(){

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.lin_fragment, fragment);
        transaction.commit();
    }
}
