package com.appzoc.shoolapp.Activites;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.appzoc.shoolapp.Fragments.ResultFragment;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;
import java.util.List;

public class Result extends AppCompatActivity implements View.OnClickListener{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Toolbar toolbar;
    ImageView img_back;
    int tabpos=0;
    ArrayList<String> tabtiles,tabids;
   // ViewPagerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initilize();
    }

    public void initilize(){
        img_back=(ImageView)findViewById(R.id.img_back);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabtiles=new ArrayList<>();


        loadArray();
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

/*
        tabids=getIntent().getStringArrayListExtra("tabids");
        tabtiles=getIntent().getStringArrayListExtra("tabtitle");
        tabpos=getIntent().getIntExtra("FragmentPosition",0);*/


        viewPager.setCurrentItem(tabpos);
        img_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
      int id=view.getId();

      switch (id){
          case R.id.img_back:{
              onBackPressed();
          }break;
      }
    }

    public void loadArray(){
        tabtiles.clear();
        for(int i=1;i<4;i++) {
            tabtiles.add("term"+i);
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        int count = tabtiles.size();
        for (int i=0; i<count; i++){

//            Log.e("TAG",tabids.get(i)+"");

           /* Bundle bundle = new Bundle();

            bundle.putString("CatID",tabids.get(i)+"");
*/
            ResultFragment fView = new ResultFragment();
          //  fView.setArguments(bundle);
            adapter.addFrag(fView,tabtiles.get(i));
        }

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
