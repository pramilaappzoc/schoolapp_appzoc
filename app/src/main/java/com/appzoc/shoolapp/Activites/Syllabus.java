package com.appzoc.shoolapp.Activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.appzoc.shoolapp.Adapters.NotificationAdapter;
import com.appzoc.shoolapp.Adapters.SyllabusAdapter;
import com.appzoc.shoolapp.Adapters.SyllabusSpinnerAdapter;
import com.appzoc.shoolapp.Models.NotiData;
import com.appzoc.shoolapp.Models.SchoolMenu;
import com.appzoc.shoolapp.Models.syllabusData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

public class Syllabus extends AppCompatActivity implements View.OnClickListener{

Spinner spin_syllabus;
ArrayList<String> arr_subject;
    RecyclerView recyclerview_syllabus;
    SyllabusAdapter adapter;
    ArrayList<syllabusData> arr_syllabus=new ArrayList<>();
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus);
        initilize();
    }

    public void initilize(){
        spin_syllabus=(Spinner) findViewById(R.id.spin_syllabus);
        arr_subject=new ArrayList<>();
        recyclerview_syllabus=(RecyclerView)findViewById(R.id.recyclerview_syllabus);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        setAdapter();
        setSubAdapter();

    }

    public void setAdapter(){
        adapter=new SyllabusAdapter(this,arr_syllabus);
        recyclerview_syllabus.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerview_syllabus.setAdapter(adapter);
    }


    public void setSubAdapter(){
        loadArray();

        SyllabusSpinnerAdapter adapter = new SyllabusSpinnerAdapter(Syllabus.this,R.layout.syllabus_spin_item,arr_subject);

//adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_syllabus.setAdapter(adapter);

        spin_syllabus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
               /* String item = ((TextView)view.findViewById(R.id.offer_type_txt)).getText().toString();
                selectedOffer.setText(item);*/
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void loadArray(){
        arr_subject.clear();
        arr_subject.add("Physics");
        arr_subject.add("English");
        arr_subject.add("Social");
        arr_subject.add("Maths");
        arr_subject.add("Biology");
        arr_subject.add("Chemistry");
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        switch (id){
            case R.id.img_back:{
                onBackPressed();
            }
        }
    }
}
