package com.appzoc.shoolapp.Activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.R;

import static com.appzoc.shoolapp.Utils.isValidPhone;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener{
EditText edt_mobile;
Button btn_reset;
ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initilize();
    }

    public void initilize(){
        btn_reset=(Button)findViewById(R.id.btn_reset);
        edt_mobile=(EditText)findViewById(R.id.edt_mobile);
        img_back=(ImageView)findViewById(R.id.img_back);

        btn_reset.setOnClickListener(this);
        edt_mobile.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        switch (id){

            case R.id.btn_reset:{
                checkAndReset();
            }break;

            case R.id.edt_mobile:{
                edt_mobile.setError(null);
            }break;
            case R.id.img_back:{
                onBackPressed();
            }break;
        }
    }

    public void checkAndReset(){
        if(edt_mobile.getText().toString().trim().equals("")){
            edt_mobile.setError("Enter The Mobile Number");
        }else if(!isValidPhone(edt_mobile.getText().toString().trim())){
            edt_mobile.setError("Enter a Valid Mobile Number");
        }else {
            /*Intent i = new Intent(ForgotPassword.this, MainActivity.class);
            startActivity(i);*/
            finish();
        }
    }
}
