package com.appzoc.shoolapp.Activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.Adapters.CalendarAdapter;
import com.appzoc.shoolapp.Adapters.EventAdapter;
import com.appzoc.shoolapp.ExpandableHeightGridView;
import com.appzoc.shoolapp.Models.CalendarData;
import com.appzoc.shoolapp.Models.EventData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Attendance extends AppCompatActivity implements View.OnClickListener{
    public GregorianCalendar cal_month, cal_month_copy;
    private CalendarAdapter cal_adapter;
    private TextView tv_month;
    ArrayList<CalendarData> date_collection_arr=new ArrayList<>();
    ImageButton previous,next;
    ExpandableHeightGridView gridview;
    RecyclerView event_recyclerview;
    ArrayList<EventData> arr_event=new ArrayList<>();
    EventAdapter event_adapter;
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        initilize();

    }

    static
    {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void initilize(){
        img_back=(ImageView)findViewById(R.id.img_back);
        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        tv_month = (TextView) findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        previous = (ImageButton) findViewById(R.id.ib_prev);
        next = (ImageButton) findViewById(R.id.Ib_next);
        gridview = (ExpandableHeightGridView) findViewById(R.id.gv_calendar);
        gridview.setExpanded(true);
        event_recyclerview=(RecyclerView)findViewById(R.id.event_recyclerview);
        event_recyclerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
        event_recyclerview.setLayoutManager(layoutManager);

        previous.setOnClickListener(this);
        next.setOnClickListener(this);
        img_back.setOnClickListener(this);

        setAdapter();
        setEventRecycleAdapter();
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();

        switch (id){
            case R.id.ib_prev:{
                setPreviousMonth();
                refreshCalendar();
            }break;

            case R.id.Ib_next:{
                setNextMonth();
                refreshCalendar();
            }break;

            case R.id.img_back:{
                onBackPressed();
            }break;
        }
    }

    public void setEventRecycleAdapter(){
        //loadMenuList();
        event_adapter=new EventAdapter(Attendance.this,arr_event);
        event_recyclerview.setAdapter(event_adapter);
    }

    public void setAdapter(){

        LoadeventArray();
        cal_adapter = new CalendarAdapter(this, cal_month, date_collection_arr);
        gridview.setAdapter(cal_adapter);
    }

    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }
    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
    }


    public void LoadeventArray(){

        date_collection_arr.clear();
        date_collection_arr.add(new CalendarData("2018-04-01","holiday"));
        date_collection_arr.add(new CalendarData("2018-04-04","leave"));
        date_collection_arr.add(new CalendarData("2018-04-06","leave"));
        date_collection_arr.add(new CalendarData("2018-05-02","holiday"));
        date_collection_arr.add(new CalendarData("2018-04-11","holiday"));
    }

}
