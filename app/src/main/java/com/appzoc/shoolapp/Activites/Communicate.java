package com.appzoc.shoolapp.Activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.appzoc.shoolapp.Adapters.CommunicationAdapter;
import com.appzoc.shoolapp.Adapters.NotificationAdapter;
import com.appzoc.shoolapp.Models.CommunicationData;
import com.appzoc.shoolapp.Models.NotiData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

public class Communicate extends AppCompatActivity implements View.OnClickListener{
    RecyclerView recyclerview_communicate;
    CommunicationAdapter adapter;
    ArrayList<CommunicationData> arr_communi=new ArrayList<>();
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communicate);
        initilize();
    }

    static
    {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void initilize(){
        recyclerview_communicate=(RecyclerView)findViewById(R.id.recyclerview_communicate);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        setAdapter();
    }

    public void setAdapter(){
        adapter=new CommunicationAdapter(arr_communi,this);
        recyclerview_communicate.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerview_communicate.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        switch (id){
            case R.id.img_back:{
                onBackPressed();
            }
        }
    }

}
