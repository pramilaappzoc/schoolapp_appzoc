package com.appzoc.shoolapp.Activites;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Fragments.AppliedRqstFragment;
import com.appzoc.shoolapp.Fragments.NewRequestFragment;
import com.appzoc.shoolapp.Fragments.PersonalDetails;
import com.appzoc.shoolapp.Fragments.ResidentialAddress;
import com.appzoc.shoolapp.R;

public class ViewProfileDetail extends AppCompatActivity implements View.OnClickListener{
    TextView txt_personal_det,txt_address;
    ImageView img_back;
    LinearLayout lin_fragment_lay;
    Fragment fragment = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile_detail);

        initilize();
    }

    public void initilize(){
        img_back=(ImageView)findViewById(R.id.img_back);
        txt_personal_det=(TextView)findViewById(R.id.txt_personal_det);
        txt_address=(TextView)findViewById(R.id.txt_address);
        lin_fragment_lay=(LinearLayout)findViewById(R.id.lin_fragment_lay);

        img_back.setOnClickListener(this);
        txt_personal_det.setOnClickListener(this);
        txt_address.setOnClickListener(this);
        setfragment();
    }

    @Override
    public void onClick(View view) {
        int id= view.getId();
        switch (id){
            case R.id.img_back:{
                onBackPressed();
            }break;
            case R.id.txt_personal_det:{
                txt_personal_det.setTextColor(getResources().getColor(R.color.black));
                txt_address.setTextColor(getResources().getColor(R.color.grey));
                fragment = new PersonalDetails();
                callFragment();
            }break;
            case R.id.txt_address:{
                txt_personal_det.setTextColor(getResources().getColor(R.color.grey));
                txt_address.setTextColor(getResources().getColor(R.color.black));
                fragment = new ResidentialAddress();
                callFragment();

            }break;


        }
    }

    public void setfragment(){

            txt_personal_det.setTextColor(getResources().getColor(R.color.black));
            txt_address.setTextColor(getResources().getColor(R.color.grey));
            fragment = new PersonalDetails();
            callFragment();
        }

    public void callFragment(){

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.lin_fragment_lay, fragment);
        transaction.commit();
    }
}
