package com.appzoc.shoolapp.Activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.appzoc.shoolapp.Adapters.NotificationAdapter;
import com.appzoc.shoolapp.Models.NotiData;
import com.appzoc.shoolapp.Models.SchoolMenu;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

public class Notification extends AppCompatActivity implements View.OnClickListener{
    RecyclerView recyclerview_noti;
    NotificationAdapter adapter;
    ArrayList<NotiData> arr_noti=new ArrayList<>();
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initilize();
    }

    public void initilize(){
        recyclerview_noti=(RecyclerView)findViewById(R.id.recyclerview_noti);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        setAdapter();
    }

    public void setAdapter(){
        adapter=new NotificationAdapter(this,arr_noti);
        recyclerview_noti.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerview_noti.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        switch (id){
            case R.id.img_back:{
                onBackPressed();
            }
        }
    }
}
