package com.appzoc.shoolapp.Activites;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.appzoc.shoolapp.Adapters.MailSubAdapter;
import com.appzoc.shoolapp.Adapters.SchoolMenuAdapter;
import com.appzoc.shoolapp.Models.SchoolMenu;
import com.appzoc.shoolapp.R;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;


import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener
        {
            DrawerLayout drawer;
            CircleImageView child_acc;
            CircleImageView profileimage;
            com.github.clans.fab.FloatingActionButton fab_add;
            SchoolMenuAdapter adapter;
            RecyclerView schl_menu_recyclerview;
            ArrayList<SchoolMenu> arr_schlMenu=new ArrayList<>();
            CardView cardview_mail;
            ImageView img_notification;
            FloatingActionsMenu multiple_pro_menu;
            String AddProfile="add";
            FrameLayout relchild_clk;
            FloatingActionButton but;
            ArrayList<String> arr_mailSub;
            int previousSelectedPosition = -1;
            LinearLayout lin_attendance,lin_timetab,lin_fees,lin_syllabus,lin_lvCal,lin_Commun,lin_gallery,logout;

            private static final int[] ITEM_DRAWABLES = { R.drawable.attendancep, R.drawable.defaultimage,
                    R.drawable.defaultimage, R.drawable.defaultimage, R.drawable.defaultimage, R.drawable.defaultimage };
            @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

          AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


                initilize();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

                toggle.setDrawerIndicatorEnabled(false);
                toggle.setHomeAsUpIndicator(R.drawable.ic_nav_slide);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
                toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (drawer.isDrawerVisible(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        } else {
                            drawer.openDrawer(GravityCompat.START);
                        }
                    }
                });




  /*      NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/


    }

            static
            {
                AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            }

    public void initilize(){
        img_notification=(ImageView)findViewById(R.id.img_notification);
        child_acc=(CircleImageView)findViewById(R.id.child_acc);
        profileimage=(CircleImageView) findViewById(R.id.profileimage);
        fab_add= (com.github.clans.fab.FloatingActionButton) findViewById(R.id.fab_add);
        cardview_mail=(CardView)findViewById(R.id.cardview_mail);
        multiple_pro_menu=(FloatingActionsMenu)findViewById(R.id.multiple_pro_menu);
        relchild_clk=(FrameLayout)findViewById(R.id.relchild_clk);

        /*navigation views*/
        lin_attendance=(LinearLayout)findViewById(R.id.lin_attendance);
        lin_timetab=(LinearLayout)findViewById(R.id.lin_timetab);
        lin_fees=(LinearLayout)findViewById(R.id.lin_fees);
        lin_syllabus=(LinearLayout)findViewById(R.id.lin_syllabus);
        lin_lvCal=(LinearLayout)findViewById(R.id.lin_lvCal);
        lin_Commun=(LinearLayout)findViewById(R.id.lin_Commun);
        logout=(LinearLayout)findViewById(R.id.logout);
        lin_gallery=(LinearLayout)findViewById(R.id.lin_gallery);





        schl_menu_recyclerview = (RecyclerView)findViewById(R.id.schl_menu_recyclerview);
        schl_menu_recyclerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
        schl_menu_recyclerview.setLayoutManager(layoutManager);

        img_notification.setOnClickListener(this);
        cardview_mail.setOnClickListener(this);
        profileimage.setOnClickListener(this);
        child_acc.setOnClickListener(MainActivity.this);
        fab_add.setOnClickListener(this);
        //relchild_clk.setOnClickListener(MainActivity.this);

        lin_attendance.setOnClickListener(MainActivity.this);
        lin_timetab.setOnClickListener(MainActivity.this);
        lin_fees.setOnClickListener(MainActivity.this);
        lin_syllabus.setOnClickListener(MainActivity.this);
        lin_lvCal.setOnClickListener(MainActivity.this);
        lin_Commun.setOnClickListener(MainActivity.this);
        lin_gallery.setOnClickListener(MainActivity.this);
        logout.setOnClickListener(MainActivity.this);


        setAdapter();


    }

    public void setAdapter(){
        loadMenuList();
        adapter=new SchoolMenuAdapter(MainActivity.this,arr_schlMenu);
        schl_menu_recyclerview.setAdapter(adapter);
    }

    public void loadMenuList()
    {
      arr_schlMenu.clear();
        SchoolMenu menu;
        menu = new SchoolMenu( getString(R.string.attendance),R.drawable.attendancep);
        arr_schlMenu.add(menu);

        menu = new SchoolMenu( getString(R.string.time_tab),R.drawable.timetable);
        arr_schlMenu.add(menu);

        menu = new SchoolMenu( getString(R.string.communication),R.drawable.communicate);
        arr_schlMenu.add(menu);

        menu = new SchoolMenu( getString(R.string.results),R.drawable.results);
        arr_schlMenu.add(menu);

        menu = new SchoolMenu( getString(R.string.syllabus),R.drawable.syllabus);
        arr_schlMenu.add(menu);

        menu = new SchoolMenu( getString(R.string.image_gallery),R.drawable.img_gallery);
        arr_schlMenu.add(menu);

        menu = new SchoolMenu( getString(R.string.leave_calendar),R.drawable.leave_calendar);
        arr_schlMenu.add(menu);

        menu = new SchoolMenu( getString(R.string.fees),R.drawable.fees);
        arr_schlMenu.add(menu);

    }

    @Override
     public void onClick(View view) {

                int id = view.getId();
                switch (id) {
                    case R.id.child_acc: {
                        MultipleAccounts();
                       // Toast.makeText(this, "profile clicked", Toast.LENGTH_SHORT).show();
                    }
                    break;
                    case R.id.profileimage:{
                        Intent i=new Intent(MainActivity.this,ViewProfileDetail.class);
                        startActivity(i);

                    }break;

                    case R.id.fab_add:{
                  Intent i=new Intent(MainActivity.this,AddNew.class);
                   startActivity(i);

                    }break;

                    case R.id.cardview_mail:{
                        sendemail();

                    }break;

                    case R.id.img_notification:{
                        Intent i=new Intent(MainActivity.this,Notification.class);
                        startActivity(i);

                    }break;


                    case R.id.lin_attendance:{
                        Intent i=new Intent(MainActivity.this, Attendance.class);
                        startActivity(i);
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }break;
                    case R.id.lin_timetab:{
                        Intent i=new Intent(MainActivity.this, TimeTable.class);
                        startActivity(i);
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }break;
                    case R.id.lin_fees:{
                        Intent i=new Intent(MainActivity.this, Fees.class);
                        startActivity(i);
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }break;

                    case R.id.lin_syllabus:{
                        Intent i=new Intent(MainActivity.this, Syllabus.class);
                        startActivity(i);
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }break;

                    case R.id.lin_lvCal:{
                        Intent i=new Intent(MainActivity.this, LeaveCalendar.class);
                        startActivity(i);
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }break;

                    case R.id.lin_Commun:{
                        Intent i=new Intent(MainActivity.this, Communicate.class);
                        startActivity(i);
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }break;
                    case R.id.lin_gallery:{
                        Intent i=new Intent(MainActivity.this, Gallery.class);
                        startActivity(i);
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        }

                    }break;

                    case R.id.logout:{
                        Toast.makeText(this, "You are logged out", Toast.LENGTH_SHORT).show();
                       finish();

                    }break;


                }
            }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

            private void popupthankyou() {
                final Dialog dialog = new Dialog(MainActivity.this, R.style.MyDialogTheme);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);

                LayoutInflater m_inflater = LayoutInflater.from(MainActivity.this);
                View m_view = m_inflater.inflate(R.layout.submited_popup, null);
                TextView txt_submit=(TextView) m_view.findViewById(R.id.txt_submit);


                ImageView submit = (ImageView) m_view.findViewById(R.id.img_submit);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                        /*Intent intent=new Intent(ChatRoomActivity.this,AllTasksActivity.class);
                        startActivity(intent);*/

                    }
                });
                dialog.setContentView(m_view);
                dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        dialog.getWindow().setBackgroundDrawableResource(R.color.translucentblue);
                dialog.show();

            }


            public void sendemail() {


                final Dialog m_dialog = new Dialog(MainActivity.this);
                m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                LayoutInflater m_inflater = LayoutInflater.from(MainActivity.this);
                View m_view = m_inflater.inflate(R.layout.dialog_sendmail, null);


                final Button btn_submit = (Button) m_view.findViewById(R.id.btn_submit);
                final EditText query_edtx=(EditText)m_view.findViewById(R.id.query_edtx);

                RecyclerView mail_sub_recyclerview=(RecyclerView)m_view.findViewById(R.id.mail_sub_recyclerview);
                mail_sub_recyclerview.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),3);
                mail_sub_recyclerview.setLayoutManager(layoutManager);
                arr_mailSub=new ArrayList<>();
                LoadMailSub();
                MailSubAdapter mail_adapter=new MailSubAdapter(MainActivity.this,arr_mailSub);
                mail_sub_recyclerview.setAdapter(mail_adapter);



                btn_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        m_dialog.dismiss();
                        popupthankyou();
                    }
                });



                m_dialog.setCancelable(false);
                m_dialog.setContentView(m_view);
                Window window = m_dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                window.setBackgroundDrawableResource(android.R.color.transparent);

                //m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                m_dialog.show();


            }

            public void LoadMailSub(){

                arr_mailSub.clear();
                arr_mailSub.add("#fees");
                arr_mailSub.add("#fees");
                arr_mailSub.add("#fees");
                arr_mailSub.add("#fees");
                arr_mailSub.add("#fees");
                arr_mailSub.add("#fees");
                arr_mailSub.add("#fees");
                arr_mailSub.add("#fees");
            }

            public void MultipleAccounts(){

                if(AddProfile.equalsIgnoreCase("add")) {
                 // Toast.makeText(this, "adding", Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < 3; i++) {
                        but = new FloatingActionButton(MainActivity.this);
                        but.setIcon(R.drawable.defaultimage);
                        but.setColorNormal(Color.WHITE);
                        but.setColorPressed(Color.WHITE);
                        but.setId(i + 1);

                        multiple_pro_menu.addButton(but);
                        but.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Log.i("TAG", "The id is : " + view.getId());
                             //   Toast.makeText(MainActivity.this, "The id is : " + view.getId(), Toast.LENGTH_SHORT).show();
                                if (multiple_pro_menu.isExpanded()) {
                                    multiple_pro_menu.collapse();
                                } else {
                                    multiple_pro_menu.expand();
                                }
                            }
                        });


                    }

                    AddProfile ="added";
                }



                if(multiple_pro_menu.isExpanded()) {
                    multiple_pro_menu.collapse();
                }else {
                    multiple_pro_menu.expand();
                }

            }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/





  /*  @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/
}
