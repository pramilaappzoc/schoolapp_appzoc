package com.appzoc.shoolapp.Activites;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Fragments.AppliedRqstFragment;
import com.appzoc.shoolapp.Fragments.NewRequestFragment;
import com.appzoc.shoolapp.Fragments.PaidFeeFragment;
import com.appzoc.shoolapp.Fragments.PendingFeeFragment;
import com.appzoc.shoolapp.R;

public class Fees extends AppCompatActivity implements View.OnClickListener{
LinearLayout lin_fragment;
TextView txt_pendingFee,txt_paidFee;
    ImageView img_back;
    Fragment fragment = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fees);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initilize();
    }

    public void initilize(){
        txt_pendingFee=(TextView)findViewById(R.id.txt_pendingFee);
        txt_paidFee=(TextView)findViewById(R.id.txt_paidFee);
        lin_fragment=(LinearLayout)findViewById(R.id.lin_fragment);
        img_back=(ImageView)findViewById(R.id.img_back);

        txt_pendingFee.setOnClickListener(this);
        txt_paidFee.setOnClickListener(this);
        img_back.setOnClickListener(this);

        setfragment();
    }

    @Override
    public void onClick(View view) {
        int id= view.getId();
        switch (id){
            case R.id.txt_pendingFee :{
                txt_pendingFee.setTextColor(getResources().getColor(R.color.black));
                txt_paidFee.setTextColor(getResources().getColor(R.color.grey));
                fragment = new PendingFeeFragment();
                callFragment();

            }break;

            case R.id.txt_paidFee:{
                txt_pendingFee.setTextColor(getResources().getColor(R.color.grey));
                txt_paidFee.setTextColor(getResources().getColor(R.color.black));
                fragment = new PaidFeeFragment();
                callFragment();
            }break;

            case R.id.img_back:{
                onBackPressed();
            }break;

        }
    }

    public void setfragment(){

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            String getName = (String) bd.get("callfragement");
            if(getName.equals("1")){
                txt_pendingFee.setTextColor(getResources().getColor(R.color.grey));
                txt_paidFee.setTextColor(getResources().getColor(R.color.black));
                fragment = new PaidFeeFragment();
                callFragment();
            }else{
                txt_pendingFee.setTextColor(getResources().getColor(R.color.black));
                txt_paidFee.setTextColor(getResources().getColor(R.color.grey));
                fragment = new PendingFeeFragment();
                callFragment();
            }
        }else{
            txt_pendingFee.setTextColor(getResources().getColor(R.color.black));
            txt_paidFee.setTextColor(getResources().getColor(R.color.grey));
            fragment = new PendingFeeFragment();
            callFragment();
        }

    }

    public void callFragment(){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.lin_fragment_lay, fragment);
        transaction.commit();
    }
}
