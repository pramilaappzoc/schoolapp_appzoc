package com.appzoc.shoolapp.Activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.R;

public class AddNew extends AppCompatActivity implements View.OnClickListener{
ImageView img_back;
Button btn_addNew;
TextView txt_forgotpass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new);
        initilize();
    }

    public void initilize(){
        img_back=(ImageView)findViewById(R.id.img_back);
        btn_addNew=(Button)findViewById(R.id.btn_addNew);
        txt_forgotpass=(TextView)findViewById(R.id.txt_forgotpass);

        img_back.setOnClickListener(this);
        btn_addNew.setOnClickListener(this);
        txt_forgotpass.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back:{
                onBackPressed();
            }break;
            case R.id.btn_addNew:{
                onBackPressed();
            }break;
            case R.id.txt_forgotpass:{
                Intent i=new Intent(AddNew.this,ForgotPassword.class);
                startActivity(i);
                finish();
            }


        }
    }
}
