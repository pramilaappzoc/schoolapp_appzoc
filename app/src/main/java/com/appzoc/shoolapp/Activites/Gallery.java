package com.appzoc.shoolapp.Activites;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.appzoc.shoolapp.Adapters.GalleryEventAdapter;
import com.appzoc.shoolapp.Fragments.PaidFeeFragment;
import com.appzoc.shoolapp.Fragments.PendingFeeFragment;
import com.appzoc.shoolapp.Fragments.ViewMoreImages;
import com.appzoc.shoolapp.Interfaces.CallMoreImages;
import com.appzoc.shoolapp.Models.GalleryEventData;
import com.appzoc.shoolapp.Models.ImageItemData;
import com.appzoc.shoolapp.R;

import java.util.ArrayList;

public class Gallery extends AppCompatActivity implements View.OnClickListener,CallMoreImages{
RecyclerView gallery_recycler_view;
ArrayList<GalleryEventData> galleryEven_arr;
    GalleryEventAdapter adapter;
    ImageView img_back;
    LinearLayout lin_frag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initilize();
    }


    public void initilize(){
        lin_frag=(LinearLayout)findViewById(R.id.lin_frag);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        galleryEven_arr = new ArrayList<GalleryEventData>();
        createDummyData();
        gallery_recycler_view = (RecyclerView) findViewById(R.id.gallery_recycler_view);
        gallery_recycler_view.setHasFixedSize(true);

        adapter = new GalleryEventAdapter( galleryEven_arr,this);
        gallery_recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        gallery_recycler_view.setAdapter(adapter);

    }

    public void createDummyData() {
        for (int i = 1; i <= 5; i++) {

            GalleryEventData dm = new GalleryEventData();

            dm.setEventTitle("Anual Day 201"+ i);

            ArrayList<ImageItemData> imageItem = new ArrayList<ImageItemData>();
            for (int j = 0; j <= 5; j++) {
                imageItem.add(new ImageItemData("image "+ j, "URL "+ j));
            }

            dm.setImageItemArr(imageItem);

            galleryEven_arr.add(dm);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.img_back:{

                onBackPressed();

            }break;

        }
    }

    @Override
    public void mcallMoreImages(int position) {
        lin_frag.setVisibility(View.VISIBLE);
        gallery_recycler_view.setVisibility(View.GONE);

        ViewMoreImages fragment = new ViewMoreImages();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.lin_frag, fragment);
        transaction.commit();
    }
}
