package com.appzoc.shoolapp.Activites;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appzoc.shoolapp.Fragments.ClassTimeTabFragment;
import com.appzoc.shoolapp.Fragments.ExamSchedule;
import com.appzoc.shoolapp.R;

public class TimeTable extends AppCompatActivity implements View.OnClickListener{
    TextView txt_Cls_timeTab,txt_examSched;
    LinearLayout lin_fragment;
    Fragment fragment = null;
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table);
        initilize();
    }

    public void initilize(){
        txt_Cls_timeTab=(TextView)findViewById(R.id.txt_Cls_timeTab);
        txt_examSched=(TextView)findViewById(R.id.txt_examSched);
        lin_fragment=(LinearLayout)findViewById(R.id.lin_fragment);
        img_back=(ImageView)findViewById(R.id.img_back);

        txt_Cls_timeTab.setOnClickListener(this);
        txt_examSched.setOnClickListener(this);
        img_back.setOnClickListener(this);

        setfragment();

    }

    @Override
    public void onClick(View view) {
        int id= view.getId();
        switch (id){
            case R.id.txt_Cls_timeTab :{
                txt_Cls_timeTab.setTextColor(getResources().getColor(R.color.black));
                txt_examSched.setTextColor(getResources().getColor(R.color.grey));
                fragment = new ClassTimeTabFragment();
                callFragment();

            }break;

            case R.id.txt_examSched:{
                txt_Cls_timeTab.setTextColor(getResources().getColor(R.color.grey));
                txt_examSched.setTextColor(getResources().getColor(R.color.black));
                fragment = new ExamSchedule();
                callFragment();
            }break;

            case R.id.img_back:{
                onBackPressed();
            }break;

        }

    }

    public void setfragment(){
            txt_Cls_timeTab.setTextColor(getResources().getColor(R.color.black));
            txt_examSched.setTextColor(getResources().getColor(R.color.grey));
            fragment = new ClassTimeTabFragment();
            callFragment();
    }

    public void callFragment(){

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.lin_fragment, fragment);
        transaction.commit();
    }
}
