package com.appzoc.shoolapp.Activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.Fragments.NewRequestFragment;
import com.appzoc.shoolapp.R;

public class SyllabusInDetail extends AppCompatActivity implements View.OnClickListener{
TextView txt_toolTitle;
ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus_in_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initilize();
    }


    public void initilize() {
        txt_toolTitle=(TextView)findViewById(R.id.txt_toolTitle);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            String getSubject = (String) bd.get("subject_name");
            if (!getSubject.equals("")) {
                txt_toolTitle.setText(getSubject);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.img_back: {
               onBackPressed();

            }
            break;

        }
    }
}
