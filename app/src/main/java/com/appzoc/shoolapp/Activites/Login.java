package com.appzoc.shoolapp.Activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.appzoc.shoolapp.R;

import static com.appzoc.shoolapp.Utils.isValidPhone;

public class Login extends AppCompatActivity implements View.OnClickListener{
Button btn_login;
TextView txt_forgotPass;
EditText edt_mobile,edt_schlCode,edt_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initilize();

    }

    public void initilize(){
        txt_forgotPass=(TextView)findViewById(R.id.txt_forgotPass);
        btn_login=(Button)findViewById(R.id.btn_login);
        edt_mobile=(EditText)findViewById(R.id.edt_mobile);
        edt_schlCode=(EditText)findViewById(R.id.edt_schlCode);
        edt_password=(EditText)findViewById(R.id.edt_password);


        txt_forgotPass.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        edt_mobile.setOnClickListener(this);
        edt_schlCode.setOnClickListener(this);
        edt_password.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id=view.getId();
        switch (id){
            case R.id.txt_forgotPass:{
                Intent i=new Intent(Login.this,ForgotPassword.class);
                startActivity(i);
            };break;
            case R.id.btn_login:{
                checkAndLogin();
            }break;

            case R.id.edt_mobile:{
                edt_mobile.setError(null);
            }break;

            case R.id.edt_schlCode:{
                edt_schlCode.setError(null);
            }break;
            case R.id.edt_password:{
                edt_password.setError(null);
            }break;


        }
    }


    public void checkAndLogin(){
        if(edt_mobile.getText().toString().trim().equals("")){
            edt_mobile.setError("Enter The Mobile Number");
        }else if(!isValidPhone(edt_mobile.getText().toString().trim())){
            edt_mobile.setError("Enter a Valid Mobile Number");
        }else if(edt_schlCode.getText().toString().trim().equals("")) {
            edt_mobile.setError(null);
            edt_schlCode.setError("Enter The School Code");

        }else if(edt_password.getText().toString().trim().equals("")) {
            edt_mobile.setError(null);
            edt_schlCode.setError(null);
            edt_password.setError("Enter The Password");
        }else{
            edt_mobile.setError(null);
            edt_schlCode.setError(null);
            edt_password.setError(null);

            Intent i = new Intent(Login.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }
}
