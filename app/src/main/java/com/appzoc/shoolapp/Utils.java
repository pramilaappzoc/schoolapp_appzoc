package com.appzoc.shoolapp;

import java.util.regex.Pattern;

/**
 * Created by appzoc on 2/5/18.
 */

public class Utils {

    public static boolean isValidPhone(String phone)
    {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone))
        {
            if(phone.length() < 6 || phone.length() > 13)
            {
                check = false;

            }
            else
            {
                check = true;

            }
        }
        else
        {
            check=false;
        }
        return check;
    }
}
