package com.appzoc.zocbase.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.animation.Animation;

import com.appzoc.zocbase.utilities.SharedPreferenceHelper;

/**
 * Created by appzoc on 22/8/15.
 */
public class BaseFragment extends Fragment {

    private SharedPreferenceHelper sharedPreferenceHelper;
    private OnFragmentSwitchListener onFragmentSwitchListener;

    public OnFragmentSwitchListener getFragmentSwitchListener() {
        return onFragmentSwitchListener;
    }

    public void setFragmentSwitchListener(OnFragmentSwitchListener azFragmentSwitchListener) {
        this.onFragmentSwitchListener = azFragmentSwitchListener;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentUtils.sDisableFragmentAnimations) {
            Animation a = new Animation() {
            };
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
//            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        onFragmentSwitchListener = (OnFragmentSwitchListener) childFragment;
    }

    public SharedPreferenceHelper getSharedPreferenceHelper() {
        if (sharedPreferenceHelper == null) {
            sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        }
        return sharedPreferenceHelper;
    }

    public void setSharedPreferenceHelper(SharedPreferenceHelper sharedPreferenceHelper) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
    }

    public void initLogout(Class<?> destinationClass) {
        if (sharedPreferenceHelper == null) {
            sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        }
        sharedPreferenceHelper.clearPreferences();
        Intent azIntent = null;
        if (destinationClass != null) {
            azIntent = new Intent(getActivity(), destinationClass);
        } else {
//            azIntent = new Intent(getActivity(), LoginActivity.class);
        }
        azIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        getActivity().finish();
        startActivity(azIntent);
    }

    public interface OnFragmentSwitchListener {
        void onFragmentSwitch(Fragment fragment, boolean addToBackStack, String backStackTag, boolean replace, String screenName);
    }


}
