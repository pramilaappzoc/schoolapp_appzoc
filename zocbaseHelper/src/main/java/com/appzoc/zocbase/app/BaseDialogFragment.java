package com.appzoc.zocbase.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.appzoc.zocbase.utilities.SharedPreferenceHelper;


/**
 * Created by appzoc on 22/8/15.
 */
public class BaseDialogFragment extends DialogFragment {

    private SharedPreferenceHelper sharedPreferenceHelper;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
//            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SharedPreferenceHelper getSharedPreferenceHelper() {
        if (sharedPreferenceHelper == null) {
            sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        }
        return sharedPreferenceHelper;
    }

    public void setSharedPreferenceHelper(SharedPreferenceHelper sharedPreferenceHelper) {
        this.sharedPreferenceHelper = sharedPreferenceHelper;
    }

    public void initLogout(Class<?> destinationClass) {
        if (sharedPreferenceHelper == null) {
            sharedPreferenceHelper = new SharedPreferenceHelper(getActivity());
        }
        sharedPreferenceHelper.clearPreferences();
        Intent azIntent=null;
        if (destinationClass != null) {
            azIntent = new Intent(getActivity(), destinationClass);
        } else {
//            azIntent = new Intent(getActivity(), LoginActivity.class);
        }
        azIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        getActivity().finish();
        startActivity(azIntent);
    }



}
