package com.appzoc.zocbase.utilities.cipher;

import java.io.File;

/**
 * Created by appzoc on 10/4/16.
 */
public class CipherUtilsTest {
    public static void main(String[] args) {
        String key = "Mary has one cat1";
        File inputFile = new File("document.txt");
        File encryptedFile = new File("document.encrypted");
        File decryptedFile = new File("document.decrypted");

        try {
            CipherUtils.encrypt(key, inputFile, encryptedFile);
            CipherUtils.decrypt(key, encryptedFile, decryptedFile);
        } catch (CipherException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
