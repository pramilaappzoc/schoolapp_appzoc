package com.appzoc.zocbase.utilities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.appzoc.zocbase.R;
import com.appzoc.zocbase.utilities.cipher.CipherException;
import com.appzoc.zocbase.utilities.cipher.CipherUtils;
import com.eftimoff.androipathview.PathView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by appzoc on 20/1/16.
 */
public class ZocUtils {

    /**
     * Check for intenet connectivity
     *
     * @param context
     * @return
     */

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int MONTH_MILLIS = 30 * DAY_MILLIS;
    static PathView pathView;

    public static boolean isInternet(Context context) {
        ConnectivityManager zConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo zNetworkInfo = zConnectivityManager.getActiveNetworkInfo();

        return zNetworkInfo != null && zNetworkInfo.isConnectedOrConnecting();

    }

    /**
     * Hide keyboard from anywhere in the app
     *
     * @param context
     */
    public static void hideKeyboard(Activity context) {
        try {
            InputMethodManager inputManager = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(context.getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate MD5 hash value for the string parameter passed
     *
     * @param key
     * @return
     */
    public static String generateMD5Hash(String key) {
        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        m.update(key.getBytes(), 0, key.length());
        String hash = new BigInteger(1, m.digest()).toString(16);
        return hash;
    }

    /**
     * Generate Unix/Epoch time
     *
     * @return
     */
    public static String generateEpochTime() {
        String epochtime = null;

        try {
            epochtime = String.valueOf(System.currentTimeMillis() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return epochtime;
    }

    public static Dialog ShowLoading(Context mContext) {
        Dialog mDialog = new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.progressbarloader);
        ((Animatable) ((ImageView) mDialog.findViewById(R.id.chavara)).getDrawable()).start();
        return mDialog;
    }

    public static void noInternetConnectionDialog(Context mContext, final ChecknetServices checknetServices) {
        final Dialog progress = new Dialog(mContext);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.setContentView(R.layout.dialouge_no_internet_connection);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        Button BTN_try = (Button) progress.findViewById(R.id.BTN_try);

        ImageView IMG_close= (ImageView) progress.findViewById(R.id.IMG_close);

        IMG_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.dismiss();
            }
        });

        BTN_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progress.dismiss();

                if(checknetServices!=null)
                {
                    checknetServices.retryApi();
                }

            }
        });

        progress.show();
    }


    public static Dialog showNointernetConnection(Context mContext) {
        Dialog progress = new Dialog(mContext);
        progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progress.setContentView(R.layout.dialouge_no_internet_connection);
        progress.setCancelable(false);
        progress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return progress;
    }
    public boolean isValidEmail(CharSequence email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    /**
     * Get Application Version Number
     *
     * @param context
     * @return
     */
    public static String getAppVersionName(Context context) {

        String versionName = null;

        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return versionName;
    }

    /**
     * Get Application Version Code
     *
     * @param context
     * @return
     */
    public static int getAppVersionCode(Context context) {

        int versionCode = 0;
        try {
            versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return versionCode;
    }

    /**
     * Get OS Version
     *
     * @return
     */
    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * Check whether email is valid
     *
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email) {
        final String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Matcher matcher;
        Pattern pattern = Pattern.compile(emailPattern);

        matcher = pattern.matcher(email);

        if (matcher != null)
            return matcher.matches();
        else
            return false;
    }

    /**
     * Capitalizes each word in the string.
     *
     * @param string
     * @return
     */
    public static String capitalizeString(String string) {

        if (string == null) {
            throw new NullPointerException("String to capitalize cannot be null");
        }

        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        for (int i = 0; i < chars.length; i++) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
                found = true;
            } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'') {
                found = false;
            }
        }

        return String.valueOf(chars);
    }

    public static String getImageFilePath(Uri uri, Context context) {
        String filePath = null;
        Log.d("", "URI = " + uri);
        if (uri != null && "content".equals(uri.getScheme())) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            cursor.moveToFirst();
            filePath = cursor.getString(0);
            cursor.close();
        } else {
            filePath = uri.getPath();
        }
        Log.d("", "Chosen path = " + filePath);
        return filePath;
    }

    public static String getFileName(Uri uri, Context context) {
        File file = new File(getFilePath(uri, context));
        return file.getName();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getFilePath(final Uri uri, final Context context) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static String getTodaysDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = new Date();
        return sdf.format(date);
    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    public static String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        String hourString = "";
        if (hours < 10)
            hourString = "0" + hours;
        else
            hourString = String.valueOf(hours);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hourString).append(":")
                .append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    public static String convertTo24hr(String _12HourTime) {
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm:a");

        Date _12HourDt = null;
        try {
            _12HourDt = _12HourSDF.parse(_12HourTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return _24HourSDF.format(_12HourDt);
    }


    ///////////////Date time ago///////////////////////
    public static String getTimeAgo(String date, Context ctx) {

        if (date != null) {
            long time = 0;
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            try {
                Date d = inputFormat.parse(date);
                time = d.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }


            if (time < 1000000000000L) {
                // if timestamp given in seconds, convert to millis
                time *= 1000;
            }

            long now = System.currentTimeMillis();
            if (time <= 0) {
                return null;
            }
            if (time > now) {
                return "just now";
            }

            // TODO: localize
            final long diff = now - time;
            if (diff < MINUTE_MILLIS) {
                return "just now";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 50 * MINUTE_MILLIS) {
                return diff / MINUTE_MILLIS + " minutes ago";
            } else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago";
            } else if (diff < 24 * HOUR_MILLIS) {
                return diff / HOUR_MILLIS + " hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else if (diff < 24 * DAY_MILLIS) {
                return diff / DAY_MILLIS + " days ago";
            } else {
                String result = diff / MONTH_MILLIS + " months ago";
                return result.replace("-", "");
            }
        } else {
            return "";
        }
    }

    public static void shareBitmap(Context context, Bitmap bitmap, String fileName) {
        try {
            File file = new File(context.getCacheDir(), System.currentTimeMillis() + ".png");
            if (file.exists()) {
                file.delete();
                file = new File(context.getCacheDir(), fileName + ".png");
            }
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            Log.e("failed getBitmap", String.valueOf(new RuntimeException()));
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        return bitmap;
    }

    public File getCacheFolder(Context context) {
        File cacheDir = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            cacheDir = new File(Environment.getExternalStorageDirectory(), "musaed");
            if (!cacheDir.isDirectory()) {
                cacheDir.mkdirs();
            }
        }

        if (!cacheDir.isDirectory()) {
            cacheDir = context.getCacheDir(); //get system cache folder
        }

        return cacheDir;
    }

    public File getDataFolder(Context context) {
        File dataDir = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            dataDir = new File(Environment.getExternalStorageDirectory(), "musaed");
            if (!dataDir.isDirectory()) {
                dataDir.mkdirs();
            }
        }

        if (!dataDir.isDirectory()) {
            dataDir = context.getFilesDir();
        }

        return dataDir;
    }

    public static boolean exportDB(String packageName, String dbName, boolean doEncrypt, String encryptionKey) {
        File destinationDirectory = new File(Environment.getExternalStorageDirectory().toString() + "/Musaed");
        File data = Environment.getDataDirectory();
        FileChannel source = null;
        FileChannel destination = null;
        String currentDBPath = "/data/" + packageName + "/databases/" + dbName;
        String backupDBName = "musaed";
        if (!destinationDirectory.exists()) {
            destinationDirectory.mkdir();
        }
        File currentDBFile = new File(data, currentDBPath);
        File backupDBFile = new File(destinationDirectory, backupDBName + getTodaysDate("dd_MM_yyyy_hh_mm_ss"));
        if (doEncrypt) {
            try {
                CipherUtils.encrypt(encryptionKey, currentDBFile, backupDBFile);
                return true;
            } catch (CipherException e) {
                e.printStackTrace();
                return false;
            }
        } else {
//            try {
//                source = new FileInputStream(currentDBFile).getChannel();
//                destination = new FileOutputStream(backupDBFile).getChannel();
//                destination.transferFrom(source, 0, source.size());
//                source.close();
//                destination.close();
//                return true;
//            } catch (IOException e) {
//                e.printStackTrace();
//                return false;
//            }
            return false;
        }
    }

    public static boolean importDB(Context context, String packageName, String dbName, File dbFile, boolean doDecrypt, String encryptionKey) {
        File data = Environment.getDataDirectory();
        String destinationDBPath = "/data/" + packageName + "/databases/" + dbName;
        File destinationDBFile = new File(data, destinationDBPath);
        if (doDecrypt) {
            try {

                if (destinationDBFile.exists()) {
                    FileChannel src = new FileInputStream(dbFile).getChannel();
                    FileChannel dst = CipherUtils.decrypt(encryptionKey, dbFile, destinationDBFile).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Toast.makeText(context, "Database Restored successfully", Toast.LENGTH_SHORT).show();
                }
                return true;
            } catch (CipherException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

}
