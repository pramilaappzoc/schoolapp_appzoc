package com.appzoc.zocbase.utilities.cipher;

/**
 * Created by appzoc on 10/4/16.
 */
public class CipherException extends Exception {
    public CipherException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
